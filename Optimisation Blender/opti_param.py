import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import imread
from os import listdir
from os.path import isfile, join
from mpl_toolkits.mplot3d import Axes3D

def comparaison(img1, img2, mean_only=True):
    im1 = imread(img1)
    im2 = imread(img2)

    if len(im1.shape) == 3: im1 = im1[:,:,0]
    if len(im2.shape) == 3: im2 = im2[:,:,0]

    im1_db = im1.astype(float)
    im2_db = im2.astype(float)

    (x1, y1) = im1.shape
    (x2, y2) = im2.shape

    if x1 != x2 or y1 != y2:
        print("Les images ne sont pas de la meme taille")
        return
    
    K = im1_db - im2_db
#   K = K[1:len(K)-1, 1:len(K)-1]

    m = np.mean(K)
    if mean_only:
        return m
    s = np.std(K)

    if np.max(K) > -np.min(K):
        maxi = np.max(K)
    else:
        maxi = np.min(K)

    return (maxi, s, m)


def create_list():
    work_path = 'opti_param_img_plan/'
    texture = 'Texture.tif'
    texture_path = join(work_path, texture)
    imgs = []
    for file in listdir(work_path):
        path = join(work_path, file)
        if isfile(path):
            if file != texture:
                split = file.split('_')
                render_engine = split[0] if len(split) == 3 else split[0] + '_' + split[1]
                lum = split[1] if len(split) == 3 else split[2]
                sample = split[2][:-4] if len(split) == 3 else split[3][:-4]
                mean = abs(comparaison(texture_path, path))
                imgs.append({'render_engine':render_engine, 'lum':lum, 'sample': sample, 'mean': mean})
    return imgs


def plot_render_engine(imgs):
    render_engines = []
    for img in imgs:
        if img['render_engine'] not in render_engines:
            render_engines.append(img['render_engine'])
    for render_engine in render_engines:
        X = [img['lum'] for img in imgs if img['render_engine']==render_engine]
        Y = [img['sample'] for img in imgs if img['render_engine']==render_engine]
        Z = [img['mean'] for img in imgs if img['render_engine']==render_engine]

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.plot_trisurf(X, Y, Z)
        ax.set_xlabel('lum')
        ax.set_ylabel('sample')
        ax.set_zlabel('mean')
        ax.set_title(render_engine)
        plt.show()


def get_min(_imgs):
    render_engines = []
    for img in _imgs:
        if img['render_engine'] not in render_engines:
            render_engines.append(img['render_engine'])
    minis = []
    for render_engine in render_engines:
        imgs = [img for img in _imgs if img['render_engine'] == render_engine]
        mini = {'render_engine': render_engine, 'img': imgs[0]}
        for img in imgs:
            if img['mean'] < mini['img']['mean']:
                mini = {'render_engine': render_engine, 'img': img}
        minis.append(mini)
    return minis


if __name__ == '__main__':
    imgs = create_list()
    plot_render_engine(imgs)
    print(get_min(imgs))

