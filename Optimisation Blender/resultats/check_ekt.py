#!/usr/bin/python3

import sqlite3

x = [2**i for i in range(0, 12)]
undone = []

for i in x:
    conn = sqlite3.connect('EEVEE/EEVEE_{}.ekt'.format(i))
    cursor = conn.cursor()
    cursor.execute("""SELECT IsCalibDone FROM GlobalState;""")
    isCalibDone = cursor.fetchone()[0]
    if not isCalibDone: undone.append(i)

for f in undone: print(f)
