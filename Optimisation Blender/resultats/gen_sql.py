#!/usr/bin/python3

import numpy as np


def generate_sql(i):
    cam1 = "'E:\\rgode\\Documents\\ENS Paris-Saclay\\2A - MIP 2019-2020\\TER\\Optimisation Blender\\opti_param_img_maquette\\EEVEE_{}\\cam1\\EEVEE_{}_CAM1.tif'".format(i, i)
    cam2 = "'E:\\rgode\\Documents\\ENS Paris-Saclay\\2A - MIP 2019-2020\\TER\\Optimisation Blender\\opti_param_img_maquette\\EEVEE_{}\\cam2\\EEVEE_{}_CAM2.tif'".format(i, i)
    # Update cam1
    sql  = 'UPDATE Camera_0_1_DatumImagePath SET DatumImagePath={} WHERE _rowid_=1;\n'.format(cam1)
    sql += 'UPDATE Camera_0_1_ImagePaths SET ImagePath={} WHERE _rowid_=1;\n'.format(cam1)
    # Update cam2
    sql += 'UPDATE Camera_0_2_DatumImagePath SET DatumImagePath={} WHERE _rowid_=1;\n'.format(cam2)
    sql += 'UPDATE Camera_0_2_ImagePaths SET ImagePath={} WHERE _rowid_=1;\n'.format(cam2)
    # Calib is not done
    sql += 'UPDATE GlobalState SET IsCalibDone=0 WHERE _rowid_=1;\n'
    return sql


for i in range(0,12):
    with open('sql/gen{}.sql'.format(i), 'w') as f:
        f.write(generate_sql(i))


