#!/usr/bin/python3

import sqlite3
import matplotlib.pyplot as plt

x = [i for i in range(0,12)]
y = []

for i in x:
    conn = sqlite3.connect('EEVEE/EEVEE_{}.ekt'.format(2**i))
    cursor = conn.cursor()
    cursor.execute("""SELECT MAX(CAST(Residual AS DOUBLE)) AS maxi FROM Mesh_1_PointResiduals_0;""")
    y.append(cursor.fetchone()[0])

plt.plot([2**i for i in x], y)
plt.xlabel('sample')
plt.ylabel('residu max')
plt.show()


