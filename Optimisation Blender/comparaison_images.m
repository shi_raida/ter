    %% DESCRIPTION : This function makes the difference of two images.

    %% INPUT and OUTPUT : [max,e,moy,moy_bord]=comparaison_images(file1,file2) returns the maximum value (max),
    %% the standard deviation (e) and the mean (moy) of the difference of two images without the edges of the images.
    %% It returns the mean of the difference of two images only at the edge (moy_bord).
    %% It also displays the differences map and an histogram which shows pixels number function of grey level (for this one only,
    %% the edges are removed).
    %% The images are respectively in "file1" and "file2".

    function [max,e,moy,moy_bord]=comparaison_images(file1,file2)

    %% Retrieving datas

    Im1 = imread(file1); % Reading images
    Im2 = imread(file2);

    Im1_db= double(Im1); % Changing images class (uint8 -> double) so that their difference can take negative values
    Im2_db= double(Im2);

    [x1,y1,z1] = size(Im1); % Images size
    [x2,y2,z2] = size(Im2);
    if (x1 ~= x2)|(y1 ~= y2)
        disp('Les images ont une taille différente');
    else

        %% Difference of the two images

        K = Im1_db - Im2_db; % Subtraction of images


        %% Displayement of the differences map

        m=mean(K(:)); % Mean calculation
        s=std(K(:)); % Standard deviation calculation
        keyboard
        imshow(K) % Differences map
        colorbar
        caxis([m-3*s;m+3*s]); % Considering that grey levels have a Gaussian behavior, this will reduce the boundaries
        % of the colorbar to have a clearer result


        %% Calculation of the difference only at the edges and without the edges

        K_bord = [K(1,:);K(length(K),:);K(:,1).';K(:,length(K)).']; % Difference at the edges

        K(1,:)=[]; % Difference without the edges
        K(:,1)=[];
        K(length(K),:)=[];
        K(:,length(K))=[];


        %% Displayement of the histogram

        m=mean(K(:));
        s=std(K(:));

        bins=[]; % Creating a vector for the abscissa of the histogram
        if ((m+3*s)-floor(m+3*s))==0
            for i=m+3*s:m+3*s
                bins = [bins;i];
            end
            %endfor
        else
            for i=(m-3*s)-1:(m+3*s)+1
                bins = [bins;i];
                %endfor
                %endif
            end
        end


        figure;
        hist(K(:),bins); % Displayement of the histogram
        axis tight;

        %% Calculation of the maximum value

        if max(K(:)) > - min(K(:)) % Here, the max is the value of K furthest from 0, so it can be negative
            max = max(K(:));
        else
            max = min(K(:));
        end
        %endif


        e = s; % Standard deviation
        moy = m; % Mean
        moy_bord = mean(K_bord(:)); % Mean only for the edges

    end
    end

    %% EXAMPLE:

% close all

%addpath('');

 %file1 = 'Texture.tif';
% file2 = 'Texture2.tif';

 %[maximum,std,mean,mean_edge] = comparaison_images(file1,file2)