#!/usr/bin/python3


from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib


def readfile(filename, pixel):
    x, y = [], []
    with open(filename, 'r') as f:
        for line in f:
            coords = line.replace('\n', '').split(' ')
            if len(coords) != 2: continue
            x.append(float(coords[0])*1920*pixel)   # 1920 pixels selon X
            y.append(float(coords[1])*1080*pixel)   # 1080 pixels selon Y
    return x, y


def readfiles(filename1, filename2, pixel1, pixel2):
    x1, y1 = readfile(filename1, pixel1)
    x2, y2 = readfile(filename2, pixel2)
    return [x1, y1, x2, y2]


def common_axe(X1, Y1, X2, Y2):
    assert(X1 in ('x', 'y', 'z'))
    assert(Y1 in ('x', 'y', 'z'))
    assert(X2 in ('x', 'y', 'z'))
    assert(Y2 in ('x', 'y', 'z'))
    assert(X1 != Y1)
    assert(X2 != Y2)
    assert(X1 == 'x' or Y1 == 'x' or X2 == 'x' or Y2 == 'x')
    assert(X1 == 'y' or Y1 == 'y' or X2 == 'y' or Y2 == 'y')
    assert(X1 == 'z' or Y1 == 'z' or X2 == 'z' or Y2 == 'z')
    if X1 == X2: return X1
    if X1 == Y2: return X1
    if Y1 == X2: return Y1
    if Y1 == Y2: return Y1


def coords3D(x1, y1, x2, y2, X1, Y1, X2, Y2):
    axe = common_axe(X1, Y1, X2, Y2)
    size = min(len(x1), len(x2))
    x, y, z = [[x1[i][0], 0] for i in range(size)], [[x1[i], 0] for i in range(size)], [[x1[i], 0] for i in range(size)]
    for i in range(size):
        if X1 == 'x': x[i][1] += x1[i][1]
        if X1 == 'y': y[i][1] += x1[i][1]
        if X1 == 'z': z[i][1] += x1[i][1]
        if Y1 == 'x': x[i][1] += y1[i][1]
        if Y1 == 'y': y[i][1] += y1[i][1]
        if Y1 == 'z': z[i][1] += y1[i][1]
        if X2 == 'x': x[i][1] += x2[i][1]
        if X2 == 'y': y[i][1] += x2[i][1]
        if X2 == 'z': z[i][1] += x2[i][1]
        if Y2 == 'x': x[i][1] += y2[i][1]
        if Y2 == 'y': y[i][1] += y2[i][1]
        if Y2 == 'z': z[i][1] += y2[i][1]
        if axe == 'x': x[i][1] /= 2
        if axe == 'y': y[i][1] /= 2
        if axe == 'z': z[i][1] /= 2
    return x, y, z


def mean(L):
    S = 0
    for i in L: S += i
    return S/len(L)


def display_coords(coords):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')

    x = [coords[0][i][1] for i in range(len(coords[0]))]
    y = [coords[1][i][1] for i in range(len(coords[1]))]
    z = [coords[2][i][1] for i in range(len(coords[2]))]
    ax.scatter([x[i]-mean(x) for i in range(len(x))], 
        [y[i]-mean(y) for i in range(len(y))], 
        [z[i]-mean(z) for i in range(len(z))])

    plt.show()


def display_states(states):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.set_xlabel('\n\nX [mm]')
    ax.set_ylabel('\n\nY [mm]')
    ax.set_zlabel('\n\nZ [mm]')

    for i in range(len(states)):
        ax.scatter([states[i][0][j] - mean(states[i][0]) for j in range(len(states[i][0]))], 
            [states[i][1][j] - mean(states[i][1]) for j in range(len(states[i][1]))], 
            [states[i][2][j] - mean(states[i][2]) for j in range(len(states[i][2]))], 
            label='passage {}'.format(i))
    plt.show()


def remove_neg(x1, y1, x2, y2):
    assert(len(x1) == len(y1) == len(x2) == len(y2))
    nx1, ny1, nx2, ny2 = [], [], [], []
    for i in range(len(x1)):
        if x1[i] > 0 and y1[i] > 0 and x2[i] > 0 and y2[i] > 0:
            nx1.append((i, x1[i]))
            ny1.append((i, y1[i]))
            nx2.append((i, x2[i]))
            ny2.append((i, y2[i]))
    return nx1, ny1, nx2, ny2


def get_stop_state(coords, start, end):
    x, y, z = [], [], []
    for i in range(len(coords[0])):
        if end >= coords[0][i][0] >= start:
            x.append(coords[0][i][1])
            y.append(coords[1][i][1])
            z.append(coords[2][i][1])
    return x, y, z


def dist(c1, c2):
    return ((c2[0]-c1[0])**2 + (c2[1]-c1[1])**2 + (c2[2]-c1[2])**2)**0.5


def oscillations(state):
    maxi = 0
    for i in range(len(state[0])):
        c1 = (state[0][i], state[1][i], state[2][i])
        for j in range(len(state)):
            c2 = (state[0][j], state[1][j], state[2][j])
            d = dist(c1, c2)
            if d > maxi: maxi = d
    return maxi


def pos_error_last(states):
    error = 0
    for s1 in states:
        p1 = (s1[0][-1], s1[1][-1], s1[2][-1])
        for s2 in states:
            p2 = (s2[0][-1], s2[1][-1], s2[2][-1])
            d = dist(p1, p2)
            if d > error: error = d
    return error


def avg(L):
    return sum(L)/len(L)


def pos_error_avg(states):
    error = 0
    for s1 in states:
        p1 = (avg(s1[0]), avg(s1[1]), avg(s1[2]))
        for s2 in states:
            p2 = (avg(s2[0]), avg(s2[1]), avg(s2[2]))
            d = dist(p1, p2)
            if d > error: error = d
    return error


def repartition(filename, cumul_limit):
    data = {}
    size = 0
    with open(filename, 'r') as f:
        for line in f:
            size += 1
            key = int(float(line.replace('\n', ''))*100)/100.
            if key in data: data[key] += 1
            else:           data[key]  = 1
    x, y = [], []
    for k, v in data.items():
        x.append(k)
        y.append(v/size*100)
    maxi = (0, 0) #(x, y)
    cumul = 0 #Avant cumul_limit sur x
    for i in range(len(x)):
        if y[i] > maxi[1]: maxi = (x[i], y[i])
        if x[i] <= cumul_limit: cumul += y[i]
    print('Centre atteignant y={} en x={}\nCertitude d\'etre sous {}mm de decalage de {}%'.format(maxi[1], maxi[0], cumul_limit, cumul))
    plt.scatter(x, y)
    plt.xlabel('Deplacement en mm')
    plt.ylabel('Nombre d\'occurances')
    plt.show()


if __name__ == '__main__':
    font = {'family' : 'normal',
            'size'   : 20}
    matplotlib.rc('font', **font)

    filename1 = 'vue_cote_avec_param_cam.csv'
    filename2 = 'vue_dessus_avec_param_cam.csv'
    pixel1 = 25/70  # Taille d'un pixel sur la video 1 en mm
    pixel2 = 25/123 # Taille d'un pixel sur la video 2 en mm 
    X1 = 'x'
    Y1 = 'z'
    X2 = 'x'
    Y2 = 'y'
    x1, y1, x2, y2 = readfiles(filename1, filename2, pixel1, pixel2)
    x1 = x1[97:]
    y1 = y1[97:]
    x2 = x2[90:]
    y2 = y2[90:]
    x1, y1, x2, y2 = remove_neg(x1, y1, x2, y2)
    coords = coords3D(x1, y1, x2, y2, X1, Y1, X2, Y2)
    leftstateindices = [(146, 184), (498, 535), (849, 886), (1200, 1237), (1551, 1588), (1904, 1940)]
    centerstateindices = [(236, 273), (411, 448), (589, 624), (762, 799), (938, 975), (1113, 1150), (1289, 1325), (1464, 1501), (1640, 1677), (1816, 1853), (1992, 2028)]
    leftstates = [get_stop_state(coords, i[0]-97, i[1]-97) for i in leftstateindices]
    centerstates = [get_stop_state(coords, i[0]-97+3, i[1]-97-3) for i in centerstateindices]
    max_osc_left = max([oscillations(state) for state in leftstates])
    max_osc_center = max([oscillations(state) for state in centerstates])
    pos_error_last_left = pos_error_last(leftstates)
    pos_error_last_center = pos_error_last(centerstates)
    pos_error_avg_left = pos_error_avg(leftstates)
    pos_error_avg_center = pos_error_avg(centerstates)
    print("LEFT:\n  LAST: {}\n  AVG: {}\n\nCENTER:\n  LAST: {}\n  AVG: {}".format(pos_error_last_left, pos_error_avg_left, pos_error_last_center, pos_error_avg_center))
    display_coords(coords)
    display_states(leftstates)
    repartition('repartition_blender', max(pos_error_last_left, max(pos_error_avg_left, max(pos_error_last_center, pos_error_avg_center))))
