//Arm 3
//Sequence 4
//Mode 3
//Orientation 1
//DIO 0
#include "Kinematics.h"
#include "GlobalArm.h"
extern void IKSequencingControl(float X, float Y, float Z, float GA, float WR, int grip, int interpolate, int pause, int enable);
// We need to declare the data exchange
// variable to be volatile - the value is
// read from memory.
volatile int playState = 0; // 0 = stopped 1 = playing

void playSequence()
{
  delay(500);
  Serial.println("Sequencing Mode Active."); 
  Serial.println("Press Pushbutton  to stop");
  playState = 1;  //set playState to 1 as the sequence is now playing
    g_bIKMode = IKM_BACKHOE;
    //###########################################################//
    // SEQUENCE 1
    //###########################################################// 
    IKSequencingControl(2500 , 2048 , 2048 , 2048 , 512 , 256 , 2000 , 1000, playState);
    //###########################################################// 

    //###########################################################//
    // SEQUENCE 2
    //###########################################################// 
    IKSequencingControl(2000 , 1648 , 2048 , 1648 , 512 , 256 , 2000 , 1000, playState);
    //###########################################################// 

    //###########################################################//
    // SEQUENCE 3
    //###########################################################// 
    IKSequencingControl(1500 , 2048 , 2048 , 2048 , 512 , 256 , 2000 , 1000, playState);
    //###########################################################// 

    //###########################################################//
    // SEQUENCE 4
    //###########################################################// 
    IKSequencingControl(2000 , 1648 , 2048 , 1648 , 512 , 256 , 2000 , 1000, playState);
    //###########################################################// 

 delay(100);
 Serial.println("Pausing Sequencing Mode."); 
 delay(500);
 //uncomment this to  put the arm in sleep position after a sequence
 //PutArmToSleep();
}
