clear
close

Im_blend = imread('C:\Users\eikosim\Documents\TestBlender\Test_lumiere\Environnement\Environnement_1\Environnement_1.tif');
Im_init = imread('C:\Users\eikosim\Documents\TestBlender\Texture.tif');
Im_Db_blend= double(Im_blend); % Changement du type des images pour qu'elles puissent prendre des valeurs n�gatives
Im_Db_init= double(Im_init);

[xb,yb,zb] = size(Im_blend);
[xi,yi,zi] = size(Im_init);

if (xb ~= xi)|(yb ~= yi)
  
  disp('Les images ont une taille diff�rente');
  
else
  
  K = Im_Db_init-Im_Db_blend; % Soustraction des deux images
  m=mean(K(:)); % En consid�rant qu'on a un comportement Gaussien, va permettre de r�duire les bornes de la colorbar afin d'avoir un r�sultat plus clair visuellement
  s=std(K(:));
  imshow (K) % Affichage de l'image des r�sidus
  colorbar
  caxis([m-3*s;m+3*s]);
  saveas(gcf,sprintf('C:/Users/eikosim/Documents/TestBlender/Besoin_supp_CRfinal/Format/jpeg/Figure_diff.tif')) % enregistrement de la figure
  imwrite(uint8(abs(K)),'C:/Users/eikosim/Documents/TestBlender/Besoin_supp_CRfinal/Format/jpeg/Im_diff.tif');
  
  K_bord = [K(1,:);K(length(K),:);K(:,1).';K(:,length(K)).'];%Bords
  % Modification de K pour qu'il deviennet l'image de la diff�rence mais sans les bords
  K(1,:)=[];
  K(:,1)=[];
  K(length(K),:)=[];
  K(:,length(K))=[]; % On supprime les 1eres et derni�res ligne et colonne
  m=mean(K(:));
  s=std(K(:));
  
  bins=[]; % Petit paragraphe pour cr�er un vecteur correspondant � l'abscisse de l'histogramme (si m+3*s est situ� en 0 et 2, qu'elle que soit sa valeur la valeur limite des abscisse sera 2)
  if ((m+3*s)-floor(m+3*s))==0
    for i=m+3*s:m+3*s
      bins = [bins;i];
    endfor
  else
    for i=(m-3*s)-1:(m+3*s)+1
      bins = [bins;i];
    endfor
  endif
  
  
  figure;
  hist(K(:),bins); % Affichage de l'histogramme de niveau de gris pour l'image des diff�rences
  axis tight;
  saveas(gcf,sprintf('C:/Users/eikosim/Documents/TestBlender/Besoin_supp_CRfinal/Format/jpeg/Hist_diff.tif')) % enregistrement de la figure
  
   if max(K(:)) > - min(K(:)) % Ici, le max est la valeur de K la plus �loign�e de 0, elle peut donc �tre n�gative
    max = max(K(:))
  else
    max = min(K(:))
  endif
  
  ecarttype = s
  moyenne = m
  moyenne_bord = mean(K_bord(:))
  
endif