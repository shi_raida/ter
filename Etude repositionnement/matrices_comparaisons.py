import numpy as np
from projection_matrices import rk_decomposition, check_proj_reconstruct


def load_matrix(filename):
    Mproj = []
    with open(filename, "r")as f:
        for l in f:
            s = l.replace('\n', '').split(' ')
            Mproj.append([float(s[0]), float(s[1]), float(s[2]), float(s[3])])
    Mproj = np.array(Mproj)
    return Mproj


if __name__ == "__main__":
    blender1  = load_matrix("blender/blender.cam1.matrix")
    blender2  = load_matrix("blender/blender.cam2.matrix")
    eikotwin1 = load_matrix("eikotwin/eikotwin.cam1.matrix")
    eikotwin2 = load_matrix("eikotwin/eikotwin.cam2.matrix")

    bc1 = check_proj_reconstruct(blender1)
    bc2 = check_proj_reconstruct(blender2)
    ec1 = check_proj_reconstruct(eikotwin1)
    ec2 = check_proj_reconstruct(eikotwin2)

    bR1, bK1, bt1, bro1 = rk_decomposition(blender1,  1 if abs(bc1[0])<abs(bc1[1]) else -1)
    bR2, bK2, bt2, bro2 = rk_decomposition(blender2,  1 if abs(bc2[0])<abs(bc2[1]) else -1)
    eR1, eK1, et1, ero1 = rk_decomposition(eikotwin1, 1 if abs(ec1[0])<abs(ec1[1]) else -1)
    eR2, eK2, et2, ero2 = rk_decomposition(eikotwin2, 1 if abs(ec2[0])<abs(ec2[1]) else -1)

