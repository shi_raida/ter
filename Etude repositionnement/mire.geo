
lcl = 1.0;

Point(1) = {  0.0  ,  0.0,   0.0  , lcl};
Point(2) = { 35.0  ,  0.0,   0.0  , lcl};
Point(3) = { 35.0  , 35.0,   0.0  , lcl};
Point(4) = {  0.0  , 35.0,   0.0  , lcl};
Point(5) = {-22.496, 35.0, -26.812, lcl};
Point(6) = {-22.496,  0.0, -26.812, lcl};

Point(7)  = {  4.0  , 162.0, -10.0  , lcl};
Point(8)  = { 36.889, 162.0, -21.971, lcl};
Point(9)  = { 36.889, 197.0, -21.971, lcl};
Point(10) = {  4.0  , 197.0, -10.0  , lcl};
Point(11) = {-26.309, 197.0, -27.501, lcl};
Point(12) = {-26.309, 162.0, -27.501, lcl};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 5};
Line(5) = {5, 6};
Line(6) = {6, 1};
Line(7) = {1, 4};
Line(8) = {4, 1};

Line(9)  = { 7,  8};
Line(10) = { 8,  9};
Line(11) = { 9, 10};
Line(12) = {10, 11};
Line(13) = {11, 12};
Line(14) = {12,  7};
Line(15) = { 7, 10};
Line(16) = {10,  7};

Line Loop(17) = {1, 2, 3, 8};
Line Loop(18) = {7, 4, 5, 6};
Line Loop(19) = { 9, 10, 11, 16};
Line Loop(20) = {15, 12, 13, 14};

Plane Surface(1) = {17};
Plane Surface(2) = {18};
Plane Surface(3) = {19};
Plane Surface(4) = {20};

Physical Surface(1) = {1};
Physical Surface(2) = {2};
Physical Surface(3) = {3};
Physical Surface(4) = {4};
