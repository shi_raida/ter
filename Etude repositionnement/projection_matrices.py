# -*- coding: utf-8 -*-
# system module
import numpy as np
#from scipy.linalg import rq, norm, inv
from numpy.linalg import norm, inv

# General usage functions


def rms(x, y):
    """
    Root mean square difference of x and y array
    """
    return np.sqrt(np.mean((x - y)**2))


def rotation_matrix(alpha, beta, gamma, deg=True):
    """
    Computes 3x3 rotation matrix where alpha,beta,gamma are euler angles
    associated to axes X,Y,Z respectively. Angles can be given in degrees
    (deg=True) or radian (set deg=False).
    """
    alpha = -1.0 * alpha  # mystery.
    beta = -1.0 * beta
    gamma = -1.0 * gamma
    if deg == False:
        print("[Warning] angles were assumed to be given in radians")
    else:
        rad = np.pi / 180.0
        alpha, beta, gamma = rad * alpha, rad * \
            beta, rad * gamma  # express angles in radian
    R1 = np.array([[np.cos(gamma), -1 * np.sin(gamma), 0.0],
                   [np.sin(gamma), np.cos(gamma), 0.0], [0.0, 0.0, 1.0]])
    R2 = np.array([[np.cos(beta), 0.0, np.sin(beta)], [
                  0.0, 1.0, 0.0], [-1 * np.sin(beta), 0.0, np.cos(beta)]])
    R3 = np.array([[1.0, 0.0, 0.0], [0.0, np.cos(alpha), -
                                     1 * np.sin(alpha)], [0.0, np.sin(alpha), np.cos(alpha)]])
    return np.dot(R3, np.dot(R2, R1))

# Functions specific to test projection matrices
def rk_decomposition(Mproj, eps=1.0):
    """
    Based on Renaud C++ script
    Computes the RK decomposition of projection matrix Mproj
    eps=+-1 #Two ways to produce the decomposition
    """
    # Init
    A = Mproj[:, :3]
    b = Mproj[:, 3]
    R = np.zeros((3, 3))
    K = np.zeros((3, 3))

    a1, a2, a3 = A[0, :], A[1, :], A[2, :]

    ro = eps / norm(a3)

    u0 = ro**2 * np.dot(a1, a3)
    v0 = ro**2 * np.dot(a2, a3)

    dist_crossa1a3 = norm(np.cross(a1, a3))
    dist_crossa2a3 = norm(np.cross(a1, a3))

    theta = np.arccos(-1.0 * np.dot(np.cross(a1, a3),
                                    np.cross(a2, a3)) / (dist_crossa1a3 * dist_crossa2a3))
    alpha = ro**2 * norm(np.cross(a1, a3)) * np.sin(theta)
    beta = ro**2 * norm(np.cross(a2, a3)) * np.sin(theta)

    r3 = ro * a3
    r1 = np.cross(a2, a3) / norm(np.cross(a2, a3))
    r2 = np.cross(r3, r1)

    R[0][:] = r1
    R[1][:] = r2
    R[2][:] = r3

    K = np.zeros((3, 3))
    K[0, 0] = alpha
    K[0, 1] = -1.0 * alpha * (np.cos(theta))
    K[0, 2] = u0
    K[1, 1] = beta / np.sin(theta)
    K[1, 2] = v0
    K[2, 2] = 1.0

    t = ro * np.dot(inv(K), b)
    return R, K, t, ro

def mproj_from_rk(R, K, t, ro):
    """
    Recomputes the projection matrix from the RK decomposition given
    by the rk_decomposition function
    """
    Rt = np.zeros((4, 4))
    Rt[:3, :3] = R / ro
    Rt[:3, 3] = t / ro
    Rt[3, 3] = 1
    Kc = np.zeros((3, 4))
    Kc[:, :3] = K

    M = np.dot(Kc, Rt)

    return M


def check_proj_reconstruct(Mproj):
    """
    Makes the 2 possible RK recompsition for the Mproj matrix
    Recomputes Mproj from the 2 decompositions
    Checks that at least one recomposition is equal to the intial Mproj
    """
    # first way to decompose m
    R1, K1, t1, ro1 = rk_decomposition(Mproj)
    m1 = mproj_from_rk(R1, K1, t1, ro1)

    # second way to decompose m
    R2, K2, t2, ro2 = rk_decomposition(Mproj, -1)
    m2 = mproj_from_rk(R2, K2, t2, ro2)

    r1 = rms(m1, Mproj)
    r2 = rms(m2, Mproj)

    return r1, r2


def auto_rk_decomposition(Mproj):
    """
    Computes R and K (maybe)
    EXPERIMENTAL, DO NOT USE FOR NOW
    """
    #K, R = rq(Mproj[:, :3])
    T = np.diag(np.sign(np.diag(K)))
    K = np.dot(K, T)
    R = np.dot(T, R)
    return R, K / K[2, 2]


def get_cam_location(Mproj):
    mat = np.linalg.inv(Mproj[:, :3])
    Xb = Mproj[:, 3]
    lt = -1 * np.dot(mat, Xb)
    return lt
