\babel@toc {french}{}
\contentsline {section}{\numberline {I}Introduction}{3}
\contentsline {section}{\numberline {II}Plaque carr\IeC {\'e}e}{3}
\contentsline {subsection}{\numberline {II-1}Cr\IeC {\'e}ation de la plaque}{3}
\contentsline {subsection}{\numberline {II-2}Mise en place de la luminosit\IeC {\'e}}{7}
\contentsline {subsection}{\numberline {II-3}Choix du moteur de rendu}{8}
\contentsline {subsection}{\numberline {II-4}R\IeC {\'e}glage du nombre de samples par pixel}{9}
\contentsline {subsection}{\numberline {II-5}G\IeC {\'e}n\IeC {\'e}ration des images}{10}
\contentsline {subsection}{\numberline {II-6}Exploitation des images}{10}
\contentsline {subsection}{\numberline {II-7}Conclusion}{11}
\contentsline {section}{\numberline {III}Pi\IeC {\`e}ce avec angle droit}{12}
\contentsline {subsection}{\numberline {III-1}Mise en place de la sc\IeC {\`e}ne}{12}
\contentsline {subsection}{\numberline {III-2}G\IeC {\'e}n\IeC {\'e}ration des images}{12}
\contentsline {subsection}{\numberline {III-3}Utilisation de \textit {EikoTwin}}{13}
\contentsline {subsection}{\numberline {III-4}Exploitation des r\IeC {\'e}sultats}{15}
\contentsline {section}{\numberline {IV}Bilan}{16}
